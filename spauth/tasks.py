from celery.utils.log import get_task_logger

from controller.celery import app
from spauth.schemas import VatsimUser

logger = get_task_logger(__name__)


@app.task(bind=True)
def after_login_user(self, raw_user: str):
    user = VatsimUser.parse_raw(raw_user)

    from django.utils import timezone
    from django.contrib.auth.models import User
    django_user, django_user_created = User.objects.update_or_create(username=user.cid,
                                                                     first_name=user.personal.name_first,
                                                                     last_name=user.personal.name_last,
                                                                     email=user.personal.email)
    django_user.last_login = timezone.now()
    django_user.save(update_fields=['last_login'])

    from profiles.models import VatsimProfile

    vatsim_profile, vatsim_profile_created = VatsimProfile.objects.update_or_create(user=django_user,
                                                                                    rating_id=user.vatsim.rating.id,
                                                                                    pilot_rating_id=user.vatsim.pilotrating.id,
                                                                                    division_id=user.vatsim.division.id,
                                                                                    division_name=user.vatsim.division.name,
                                                                                    region_id=user.vatsim.region.id,
                                                                                    region_name=user.vatsim.region.name,
                                                                                    subdivision_id=user.vatsim.subdivision.id,
                                                                                    subdivision_name=user.vatsim.subdivision.name)
    print(vatsim_profile.is_portugal_subdivision_member)
    if vatsim_profile.is_portugal_subdivision_member:
        from profiles.models import VACCProfile

        vacc_profile, vacc_profile_created = VACCProfile.objects.update_or_create(vatsim_profile=vatsim_profile)
        return f"User {'created' if django_user_created or vatsim_profile_created or vacc_profile_created else 'updated'}"

    return f"User {'created' if django_user_created or vatsim_profile_created else 'updated'}"
