from typing import Optional

from pydantic import BaseModel


class VatsimUserCountry(BaseModel):
    id: str
    name: str


class VatsimUserPersonal(BaseModel):
    name_first: str
    name_last: str
    name_full: str
    email: str


class VatsimUserRating(BaseModel):
    id: int
    long: str
    short: str


class VatsimUserPilotRating(BaseModel):
    id: int
    long: str
    short: str


class VatsimUserDivision(BaseModel):
    id: str
    name: str


class VatsimUserRegion(BaseModel):
    id: str
    name: str


class VatsimUserSubdivision(BaseModel):
    id: Optional[str]
    name: Optional[str]


class VatsimUserData(BaseModel):
    rating: VatsimUserRating
    pilotrating: VatsimUserPilotRating
    division: VatsimUserDivision
    region: VatsimUserRegion
    subdivision: VatsimUserSubdivision


class VatsimUser(BaseModel):
    cid: str
    personal: VatsimUserPersonal
    vatsim: VatsimUserData
