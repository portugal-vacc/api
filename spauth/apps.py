from django.apps import AppConfig
from django.conf import settings
from supertokens_python import init, InputAppInfo, SupertokensConfig, get_all_cors_headers
from supertokens_python.recipe import session, thirdparty

from spauth.vatsimprovider import VatsimProvider


class SuperTokensConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'spauth'

    def ready(self):
        init(
            app_info=InputAppInfo(
                app_name="Portugal vACC Auth",
                api_domain="http://localhost:8000",
                website_domain="http://localhost:3000",
                api_base_path="/api/auth/",
                website_base_path="/"
            ),
            supertokens_config=SupertokensConfig(
                connection_uri="http://supertokens:3567",
                # api_key="IF YOU HAVE AN API KEY FOR THE CORE, ADD IT HERE"
            ),
            framework='django',
            recipe_list=[
                session.init(),
                thirdparty.init(
                    sign_in_and_up_feature=thirdparty.SignInAndUpFeature(providers=[VatsimProvider()])
                )
            ],
            mode='wsgi'  # wsgi/asgi
        )
        settings.CORS_ALLOW_HEADERS += get_all_cors_headers()
