from typing import Dict, Union, Any

from supertokens_python.recipe.thirdparty.provider import Provider
from supertokens_python.recipe.thirdparty.types import AccessTokenAPI, AuthorisationRedirectAPI
from supertokens_python.recipe.thirdparty.types import UserInfo, UserInfoEmail

from spauth import tasks
from spauth.services import get_vatsim_user


class VatsimProvider(Provider):
    def __init__(self):
        super().__init__('vatsim', "339", False)

    async def get_profile_info(self, auth_code_response: Dict[str, Any], user_context: Dict[str, Any]) -> UserInfo:
        user = get_vatsim_user(access_token=auth_code_response["access_token"])
        tasks.after_login_user.delay(raw_user=user.json())
        return UserInfo(user_id=user.cid,
                        email=UserInfoEmail(email=user.personal.email, email_verified=True))

    def get_authorisation_redirect_api_info(self, user_context: Dict[str, Any]) -> AuthorisationRedirectAPI:
        params: Dict[str, Any] = {
            'scope': 'full_name vatsim_details email country',
            'response_type': 'code',
            'client_id': "339",
        }
        return AuthorisationRedirectAPI(url="https://auth-dev.vatsim.net/oauth/authorize", params=params)

    def get_access_token_api_info(
            self, redirect_uri: str, auth_code_from_request: str, user_context: Dict[str, Any]) -> AccessTokenAPI:
        params = {
            'client_id': "339",
            'client_secret': "TX8eH4j1S2tBNKZTF538QQk7D1Fp3uumxUVjCksC",
            'grant_type': 'authorization_code',
            'code': auth_code_from_request,
            'redirect_uri': redirect_uri
        }
        return AccessTokenAPI(url="https://auth-dev.vatsim.net/oauth/token", params=params)

    def get_redirect_uri(self, user_context: Dict[str, Any]) -> Union[None, str]:
        return None
