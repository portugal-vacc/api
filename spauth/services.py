import httpx

from spauth.schemas import VatsimUser


def get_vatsim_user(access_token: str) -> VatsimUser:
    response = httpx.get("https://auth-dev.vatsim.net/api/user",
                         headers={"Authorization": f'Bearer {access_token}',
                                  "Accept": "application/json"})
    response.raise_for_status()
    return VatsimUser(**response.json()["data"])
