from django.db import models


class TopSkyExclusion(models.Model):
    class Meta:
        db_table = "eaup_topsky_exclusion"
        verbose_name_plural = "Top Sky Exclusions"

    country_code = models.CharField(max_length=2)
    area_name = models.CharField(max_length=20, primary_key=True)

    def save(self, *args, **kwargs):
        self.country_code = self.country_code.lower()
        self.area_name = self.area_name.lower()
        return super(TopSkyExclusion, self).save(*args, **kwargs)

    def __str__(self):
        return self.area_name
