from typing import List, Tuple, Any, Dict

from celery.utils.log import get_task_logger

from controller.celery import app
from eaup.data import notice_info_data, area_list_all_data, convert_to_eaup
from eaup.schemas import EaupInfo, EaupArea, Eaup
from eaup.web import web_latest_area_data

logger = get_task_logger(__name__)


def save_eaup_data(data: Eaup) -> None:
    with open("eaup.json", "w") as json_file:
        json_file.write(data.json())


@app.task(bind=True)
def eaup_webscrape_task(self) -> List[Tuple[str, Any]]:
    data: Dict[str, str] = web_latest_area_data()
    change_log = []
    eaup_info: EaupInfo = notice_info_data(raw_info=data["info"])
    eaup_areas: List[EaupArea] = area_list_all_data(raw_data=data["data"])
    eaup: Eaup = convert_to_eaup(info=eaup_info, area_list=eaup_areas)
    save_eaup_data(eaup)
    change_log.append(("updated", eaup_info.json()))
    return change_log
