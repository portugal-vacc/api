from typing import List

from django.http import HttpResponse
from ninja import Router

from eaup.data import area_list_country_data, read_eaup_json
from eaup.schemas import TopSkyArea, TopSky, EaupArea, parse_top_sky_areas, Eaup
from eaup.topsky.services import faa_notams

top_sky_router = Router(tags=['eaup:topsky'])


@top_sky_router.get("/", response=TopSky)
def top_sky_data(request):
    eaup: Eaup = read_eaup_json()
    top_sky_areas: List[TopSkyArea] = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup.areas)
    return TopSky(notice_info=eaup.notice_info, areas=top_sky_areas)


@top_sky_router.get("/{country_code}", response=TopSky)
def top_sky_country_data(request, country_code: str):
    eaup: Eaup = read_eaup_json()
    eaup_country_areas: List[EaupArea] = area_list_country_data(eaup_area_list=eaup.areas, country_code=country_code)
    top_sky_areas: List[TopSkyArea] = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup_country_areas)
    return TopSky(notice_info=eaup.notice_info, areas=top_sky_areas)


def top_sky_area_manual_format(area: TopSkyArea) -> str:
    return f"{area.name[2:]}:{area.start_date.strftime('%y%m%d')}:" \
           f"{area.end_date.strftime('%y%m%d')}:{area.week_days}:" \
           f"{area.start_time.strftime('%H%M')}:{area.end_time.strftime('%H%M')}:" \
           f"{area.lower}:{area.upper}:{area.user_text}"


def top_sky_area_notam_format(area: TopSkyArea) -> str:
    return f"<div><PRE>{area.user_text} NOTAMN \n" \
           f"Q) LPPC/QRALW/IV/NBO/W /{round(area.lower / 100)}/{round(area.upper / 100)}/4058N00705W041 \n" \
           f"A) LPPC B) {area.start_date.strftime('%y%m%d')}{area.start_time.strftime('%H%M')} C) {area.end_date.strftime('%y%m%d')}{area.end_time.strftime('%H%M')} \n" \
           f"D) {area.start_time.strftime('%H%M')}-{area.end_time.strftime('%H%M')} \n" \
           f"E) AIRSPACE RESERVATION WILL TAKE PLACE, AREA: {area.name} \n" \
           f"F) FL{round(area.lower / 100)} G) FL{round(area.upper / 100)} \n" \
           f"</PRE></div>\n"


@top_sky_router.get("/{country_code}/text")
def text_top_sky_country_data(request, country_code: str):
    eaup: Eaup = read_eaup_json()
    eaup_country_areas: List[EaupArea] = area_list_country_data(eaup_area_list=eaup.areas, country_code=country_code)
    top_sky_areas: List[TopSkyArea] = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup_country_areas)
    areas_as_string = (top_sky_area_manual_format(area) for area in top_sky_areas)
    return HttpResponse("<br>".join(areas_as_string))


@top_sky_router.get("/{country_code}/aupnotam/")
def text_top_sky_country_notam(request, country_code: str, loc: str):
    eaup: Eaup = read_eaup_json()
    eaup_country_areas: List[EaupArea] = area_list_country_data(eaup_area_list=eaup.areas, country_code=country_code)
    top_sky_areas: List[TopSkyArea] = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup_country_areas)
    areas_as_notam = (top_sky_area_notam_format(area) for area in top_sky_areas)
    notams = "<br>".join(areas_as_notam)
    return HttpResponse(notams + faa_notams(locations = loc))


@top_sky_router.get("/{country_code}/download")
def download_top_sky_country_data(request, country_code: str):
    eaup: Eaup = read_eaup_json()
    eaup_country_areas: List[EaupArea] = area_list_country_data(eaup_area_list=eaup.areas, country_code=country_code)
    top_sky_areas: List[TopSkyArea] = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup_country_areas)
    areas_as_string = (top_sky_area_manual_format(area) for area in top_sky_areas)
    response = HttpResponse("\n".join(areas_as_string), content_type="text/plain,charset=utf8")
    response['Content-Disposition'] = f'attachment; filename=TopSkyAreasManualAct.txt'
    return response
