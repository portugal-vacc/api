from ninja import ModelSchema, Schema

from eaup.models import TopSkyExclusion


class Message(Schema):
    message: str


class TopSkyExclusionSchema(ModelSchema):
    class Config:
        model = TopSkyExclusion
        model_fields = "__all__"
