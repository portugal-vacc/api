from typing import List

from ninja import Router

from eaup.models import TopSkyExclusion
from eaup.topsky.schemas import TopSkyExclusionSchema, Message

top_sky_exclusions = Router(tags=['eaup:topsky:exclusions'])


@top_sky_exclusions.get("/{country_code}", response=List[TopSkyExclusionSchema])
def exclusions(request, country_code: str):
    return TopSkyExclusion.objects.filter(country_code=country_code.lower())


@top_sky_exclusions.post("/{country_code}", response={200: TopSkyExclusionSchema, 409: Message})
def post_exclusion(request, country_code: str, area_name: str):
    exclusion, created = TopSkyExclusion.objects.get_or_create(country_code=country_code.lower(),
                                                               area_name=area_name.lower())
    return (200, exclusion) if created else (409, {'message': 'Exclusion already exists'})


@top_sky_exclusions.delete("/{country_code}/{area_name}", response={204: Message})
def delete_exclusion(request, country_code: str, area_name: str):
    TopSkyExclusion.objects.get(country_code=country_code.lower(), area_name=area_name.lower()).delete()
    return 204, {'message': 'Deleted'}
