import httpx

from controller.exceptions import ExternalServiceClientErrorResponse, ExternalServiceServerErrorResponse


def get_request(url: str) -> str:
    response = httpx.get(url)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse()
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse()
    return response.text


def faa_notams(locations: str):
    link = f"https://www.notams.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&reportType=RAW&formatType=DOMESTIC&retrieveLocId={locations}&actionType=notamRetrievalByICAOs"
    return get_request(link)
