from typing import List, Dict

from ninja import Router

from eaup.data import read_eaup_json, area_list_country_data, convert_to_eaup
from eaup.schemas import Eaup, EaupArea
from eaup.topsky.exclusions import top_sky_exclusions
from eaup.topsky.router import top_sky_router
from eaup.web import web_latest_area_data

eaup_router = Router(tags=['eaup'])
app_name = 'eaup'

eaup_router.add_router("/topsky/", top_sky_router)
eaup_router.add_router("/topsky/exclusions", top_sky_exclusions)


@eaup_router.get("/", response=Eaup)
def eaup_data(request):
    return read_eaup_json()


@eaup_router.get("/raw", response=Dict[str, str])
def eaup_raw_data(request):
    return web_latest_area_data()


@eaup_router.get("/{country_code}", response=Eaup)
def eaup_country_data(request, country_code: str):
    eaup: Eaup = read_eaup_json()
    eaup_country_areas: List[EaupArea] = area_list_country_data(eaup_area_list=eaup.areas, country_code=country_code)
    return convert_to_eaup(info=eaup.notice_info, area_list=eaup_country_areas)
