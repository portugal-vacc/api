# EAUP
European Airspace Use Plan

# Objective
Areas be available trough an json api

# Explanation
This process is composed of two steps:
1. Webscrape data from [NOP Eurocontrol](https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/index.html) (Selenium 4 the rescue)
2. Data verification (Pydantic 4 the rescue)
3. Data parsing (Regex 4 the rescue)
4. Data output (Django Ninja 4 the rescue)

# Notes
- Expect increased delay on first call.
- The web scrapping data is cached for 10 minutes from intial call.

# Schemas
## Eaup Output
```
{
  "notice_info": {
    "type": "string",
    "valid_wef": "2022-03-27T17:32:25.941Z",
    "valid_til": "2022-03-27T17:32:25.941Z",
    "released_on": "2022-03-27T17:32:25.941Z"
  },
  "areas": [
    {
      "name": "string",
      "minimum_fl": 0,
      "maximum_fl": 0,
      "start_time": "string",
      "end_time": "string"
    }
  ]
}
```

