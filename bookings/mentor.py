from typing import List

from ninja import Router

from bookings.bridge import mentor_bookings
from bookings.schemas.booking import BookingOut

mentor_router = Router(tags=['bookings:mentor'])


@mentor_router.get("/{mentor_id}", response=List[BookingOut])
def get_mentor_bookings(request, mentor_id: int):
    return mentor_bookings(mentor_id)
