from typing import List

from ninja import Router

from bookings.bridge import client_bookings
from bookings.schemas.booking import BookingOut

client_router = Router(tags=['bookings:client'])


@client_router.get("/{client_id}", response=List[BookingOut])
def get_client_bookings(request, client_id: int):
    return client_bookings(client_id)
