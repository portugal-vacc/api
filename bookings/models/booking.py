from datetime import datetime

from django.db import models


class Booking(models.Model):
    type: str = models.CharField(max_length=30)
    client_id: int = models.IntegerField(null=False)
    position: str = models.CharField(max_length=30)
    start_date: datetime = models.DateTimeField()
    end_date: datetime = models.DateTimeField()
    mentor: int = models.IntegerField(null=True, blank=True)
    created_at: datetime = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create(cls, type: str, client_id: int, position: str, start_date: datetime, end_date: datetime, mentor: int):
        return cls(type=type, client_id=client_id, position=position, start_date=start_date, end_date=end_date,
                   mentor=mentor)

    def __str__(self):
        return f"[{self.position}] {self.type} - {self.client_id} {f'- Mentor {self.mentor}' if self.mentor else ''}"
