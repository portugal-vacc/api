from datetime import datetime, timezone
from typing import Tuple, Dict

from django.db.models import QuerySet
from django.shortcuts import get_object_or_404

from bookings.models.booking import Booking
from bookings.schemas.booking import BookingIn, BookingModel


def future_bookings() -> QuerySet:
    return Booking.objects.filter(end_date__gte=datetime.now(tz=timezone.utc)).order_by('start_date')


def new_booking(payload: BookingIn) -> Booking:
    booking: BookingModel = payload.convert_to_model()
    booking.check_already_exists()
    return Booking.objects.create(**booking.dict())


def this_booking(booking_id: int) -> Booking:
    return get_object_or_404(Booking, id=booking_id)


def delete_this_booking(booking_id: int) -> Tuple[int, Dict[str, int]]:
    booking = get_object_or_404(Booking, id=booking_id)
    return booking.delete()


def client_bookings(client_id: int) -> QuerySet:
    return Booking.objects.filter(client_id=client_id).order_by('start_date')


def mentor_bookings(mentor_id: int) -> QuerySet:
    return Booking.objects.filter(mentor=mentor_id).order_by('start_date')
