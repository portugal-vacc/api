from django.contrib import admin

from bookings.models.booking import Booking

admin.site.register(Booking)
