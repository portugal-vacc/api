from typing import List

from ninja import Router

from bookings.bridge import future_bookings, new_booking, this_booking, delete_this_booking
from bookings.client import client_router
from bookings.mentor import mentor_router
from bookings.schemas.booking import BookingIn, BookingOut

bookings_router = Router(tags=["bookings"])
app_name = "bookings"

bookings_router.add_router("/client/", client_router)
bookings_router.add_router("/mentor/", mentor_router)


@bookings_router.get("/", response=List[BookingOut])
def get_bookings(request):
    return future_bookings()


@bookings_router.post("/")
def post_booking(request, payload: BookingIn):
    booking = new_booking(payload)
    return {"success": True} if booking else {"success": False}


@bookings_router.get("/{booking_id}", response=BookingOut)
def get_booking(request, booking_id: int):
    return this_booking(booking_id)


@bookings_router.delete("/{booking_id}")
def delete_booking(request, booking_id: int):
    action = delete_this_booking(booking_id)
    return {"success": True} if action[0] == 1 else {"success": False}
