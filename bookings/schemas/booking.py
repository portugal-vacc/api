from datetime import datetime, time, date, timezone, timedelta
from typing import Optional

from ninja import Schema, ModelSchema
from pydantic import validator, root_validator

from bookings.models.booking import Booking
from controller.exceptions import DualBookingError, MinimumBookingTimeError


class BookingModel(Schema):
    type: str
    client_id: int
    position: str
    start_date: datetime
    end_date: datetime
    mentor: Optional[str]

    @root_validator(pre=True)
    def end_time_different_than_start_time(cls, values):
        start_date, end_date = values.get('start_date'), values.get('end_date')
        delta = end_date - start_date
        if delta < timedelta(minutes=30):
            raise MinimumBookingTimeError()
        return values

    def check_already_exists(self):
        bookings = Booking.objects.all()
        for booking in bookings:
            if booking.position == self.position \
                    and self.start_date >= booking.start_date \
                    and self.end_date <= booking.end_date:
                raise DualBookingError()


class BookingIn(Schema):
    client_id: str
    position: str
    start_date: date
    start_time: time
    end_time: time
    mentor: Optional[str]

    @root_validator(pre=True)
    def end_time_different_than_start_time(cls, values):
        start_time, end_time = values.get('start_time'), values.get('end_time')
        if start_time == end_time:
            raise ValueError('Start and End time are the same...')
        return values

    @validator('start_date')
    def start_date_today_or_after(cls, v):
        assert v >= date.today(), 'Start day must be today or after'
        return v

    @property
    def type(self) -> str:
        return "Controller Training" if self.mentor else "Controller Scheduled"

    @property
    def booking_start_date(self) -> datetime:
        return datetime.combine(date=self.start_date, time=self.start_time, tzinfo=timezone.utc)

    @property
    def booking_end_date(self) -> datetime:
        _end_date = datetime.combine(date=self.start_date, time=self.end_time, tzinfo=timezone.utc)
        if self.start_time > self.end_time:
            _end_date = _end_date.replace(day=self.start_date.day + 1)
        return _end_date

    @property
    def booking_mentor(self) -> Optional[int]:
        return int(self.mentor) if self.mentor else None

    def convert_to_model(self) -> BookingModel:
        return BookingModel(type=self.type, client_id=int(self.client_id), position=self.position,
                            start_date=self.booking_start_date, end_date=self.booking_end_date,
                            mentor=self.booking_mentor)


class BookingOut(ModelSchema):
    class Config:
        model = Booking
        model_fields = "__all__"
