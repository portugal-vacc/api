from typing import List, Optional

from ninja import Router

from vatsim.schemas.events import Event
from vatsim.services import get_all_events

events_router = Router(tags=['vatsim:events'])


def parse_portugal_events(all_events: List[Optional[Event]]) -> List[Optional[Event]]:
    portugal_events: List = []
    for event in all_events:
        if [airport for airport in event.airports if airport.icao.startswith("LP")]:
            portugal_events.append(event)
    return portugal_events


@events_router.get("/portugal", response=List[Optional[Event]])
def portugal(request):
    return parse_portugal_events(get_all_events())
