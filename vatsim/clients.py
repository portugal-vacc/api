from ninja import Router

from vatsim.schemas.clients import Client, ClientConnections, ClientFlightPlans, ClientRatingTimes
from vatsim.services import get_client, get_client_connections, get_client_flight_plans, get_client_rating_times

clients_router = Router(tags=['vatsim:clients'])


@clients_router.get("/{cid}", response=Client)
def client(request, cid: int):
    return get_client(cid)


@clients_router.get("/{cid}/connections", response=ClientConnections)
def client_connections(request, cid: int):
    return get_client_connections(cid)


@clients_router.get("/{cid}/flight-plans", response=ClientFlightPlans)
def client_flight_plans(request, cid: int):
    return get_client_flight_plans(cid)


@clients_router.get("/{cid}/times", response=ClientRatingTimes)
def client_rating_times(request, cid: int):
    return get_client_rating_times(cid)
