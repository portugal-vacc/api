from typing import List, Optional

import httpx
from cachetools import TTLCache, cached
from pydantic import ValidationError

from controller.exceptions import ExternalServiceTypeVerificationError, ExternalServiceClientErrorResponse, \
    ExternalServiceServerErrorResponse
from vatsim.schemas.clients import Client, ClientConnections, ClientFlightPlans, ClientRatingTimes
from vatsim.schemas.events import Event
from vatsim.schemas.vatsimdata import VatsimData


def get_request(url: str) -> dict:
    response = httpx.get(url)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse()
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse()
    return response.json()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_all_events() -> List[Optional[Event]]:
    data = get_request('https://my.vatsim.net/api/v1/events/all')
    events = data['data']
    try:
        return [Event(**event) for event in events]
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_vatsim_data() -> VatsimData:
    data = get_request('https://data.vatsim.net/v3/vatsim-data.json')
    try:
        return VatsimData(**data)
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client(client_id: int) -> Client:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/')
    try:
        return Client(**data)
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_connections(client_id: int) -> ClientConnections:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/connections/')
    try:
        return ClientConnections(**data)
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_flight_plans(client_id: int) -> ClientFlightPlans:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/flight_plans/')
    try:
        return ClientFlightPlans(**data)
    except ValidationError:
        raise ExternalServiceTypeVerificationError()


@cached(cache=TTLCache(maxsize=16, ttl=60))
def get_client_rating_times(client_id: int) -> ClientRatingTimes:
    data = get_request(f'https://api.vatsim.net/api/ratings/{client_id}/rating_times/')
    try:
        return ClientRatingTimes(**data)
    except ValidationError:
        raise ExternalServiceTypeVerificationError()
