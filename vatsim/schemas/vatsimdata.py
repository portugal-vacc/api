from typing import List, Optional

from ninja import Schema


class VatsimDataGeneral(Schema):
    version: int
    reload: int
    update: str
    update_timestamp: str
    connected_clients: int
    unique_users: int


class VatsimDataFlightPlan(Schema):
    flight_rules: str
    aircraft: str
    aircraft_faa: Optional[str]
    aircraft_short: str
    departure: str
    arrival: str
    alternate: Optional[str]
    cruise_tas: str
    altitude: str
    deptime: str
    enroute_time: str
    fuel_time: str
    remarks: str
    route: str
    revision_id: int
    assigned_transponder: str


class VatsimDataPilot(Schema):
    cid: int
    name: str
    callsign: str
    server: str
    pilot_rating: int
    latitude: float
    longitude: float
    altitude: int
    groundspeed: int
    transponder: str
    heading: int
    qnh_i_hg: float
    qnh_mb: int
    flight_plan: Optional[VatsimDataFlightPlan]
    logon_time: str
    last_updated: str


class VatsimDataController(Schema):
    cid: int
    name: str
    callsign: str
    frequency: str
    facility: int
    rating: int
    server: str
    visual_range: int
    text_atis: Optional[List[str]]
    last_updated: str
    logon_time: str


class VatsimDataAtis(Schema):
    cid: int
    name: str
    callsign: str
    frequency: str
    rating: int
    server: str
    visual_range: int
    atis_code: Optional[str]
    text_atis: Optional[List[str]]
    last_updated: str
    logon_time: str


class VatsimDataServer(Schema):
    ident: str
    hostname_or_ip: str
    location: str
    name: str
    clients_connection_allowed: int
    client_connections_allowed: bool
    is_sweatbox: bool


class VatsimDataPrefile(Schema):
    cid: int
    name: str
    callsign: str
    flight_plan: Optional[VatsimDataFlightPlan]
    last_updated: str


class VatsimDataFacility(Schema):
    id: int
    short: str
    long: str


class VatsimDataRating(Schema):
    id: int
    short: str
    long: str


class VatsimDataPilotRating(Schema):
    id: int
    short_name: str
    long_name: str


class VatsimData(Schema):
    general: Optional[VatsimDataGeneral]
    pilots: List[Optional[VatsimDataPilot]]
    controllers: List[Optional[VatsimDataController]]
    atis: List[Optional[VatsimDataAtis]]
    servers: List[Optional[VatsimDataServer]]
    prefiles: List[Optional[VatsimDataPrefile]]
    facilities: List[Optional[VatsimDataFacility]]
    ratings: List[Optional[VatsimDataRating]]
    pilot_ratings: List[Optional[VatsimDataPilotRating]]
