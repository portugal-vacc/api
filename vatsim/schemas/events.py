from typing import Optional, List

from ninja import Schema


class EventsOrganizer(Schema):
    region: Optional[str]
    division: Optional[str]
    subdivision: Optional[str]
    organised_by_vatsim: bool


class EventsAirport(Schema):
    icao: str


class EventsRoute(Schema):
    departure: str
    arrival: str
    route: str


class Event(Schema):
    id: int
    type: str
    vso_name: Optional[str]
    name: str
    link: Optional[str]
    organizers: Optional[List[EventsOrganizer]]
    airports: List[Optional[EventsAirport]]
    routes: List[Optional[EventsRoute]]
    start_time: str
    end_time: str
    short_description: str
    description: str
    banner: str
