from ninja import Router

from vatsim.clients import clients_router
from vatsim.events import events_router
from vatsim.live import live_router

vatsim_router = Router(tags=['vatsim'])
app_name = 'vatsim'

vatsim_router.add_router("/live/", live_router)
vatsim_router.add_router("/events/", events_router)
vatsim_router.add_router("/clients/", clients_router)
