from typing import List

from ninja import Router

from vatsim.schemas.vatsimdata import VatsimDataPilot, VatsimDataController, VatsimDataGeneral, VatsimDataAtis, VatsimDataServer, VatsimDataPrefile, VatsimDataFacility, VatsimDataRating, VatsimDataPilotRating
from vatsim.services import get_vatsim_data

live_router = Router(tags=['vatsim:live'])


@live_router.get("/general", response=VatsimDataGeneral)
def general(request):
    return get_vatsim_data().general


@live_router.get("/pilots", response=List[VatsimDataPilot])
def pilots(request):
    return get_vatsim_data().pilots


@live_router.get("/controllers", response=List[VatsimDataController])
def controllers(request):
    return get_vatsim_data().controllers


@live_router.get("/atis", response=List[VatsimDataAtis])
def atis(request):
    return get_vatsim_data().atis


@live_router.get("/servers", response=List[VatsimDataServer])
def servers(request):
    return get_vatsim_data().servers


@live_router.get("/prefiles", response=List[VatsimDataPrefile])
def prefiles(request):
    return get_vatsim_data().prefiles


@live_router.get("/facilities", response=List[VatsimDataFacility])
def facilities(request):
    return get_vatsim_data().facilities


@live_router.get("/ratings", response=List[VatsimDataRating])
def ratings(request):
    return get_vatsim_data().ratings


@live_router.get("/pilot_ratings", response=List[VatsimDataPilotRating])
def pilot_ratings(request):
    return get_vatsim_data().pilot_ratings
