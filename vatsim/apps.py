from django.apps import AppConfig


class VatsimConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vatsim'
