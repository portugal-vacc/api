from django.apps import AppConfig


class NavDataConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "navdata"
