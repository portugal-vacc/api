from ninja import Router

from navdata.positions.routes import positions_router

navdata_router = Router(tags=['navdata'])
app_name = 'navdata'

navdata_router.add_router("/positions/", positions_router)
