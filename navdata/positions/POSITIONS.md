# Positions
The objective of this file is to instruct on how to update the positions located under ``positions.py``
# Note
Keep in mind... ``ratings_required`` is not implemented on this script
# Python Script
``` python
def regex_positions(text: str):
    return re.finditer(r"", text)

def main():
    with open(file, mode="r") as ese_file:
        file_text = ese_file.read()
        positions = regex_positions(file_text)
        print({position["callsign"]: position for position in positions}
``` 
# Regex
``` 
(?P<call_sign>LP\S+):(?P<name>.*):(?P<frequency>\d{3}.\d{3}):(?P<code>\S{1,4}):(?P<sector>\S{0,1}):(?P<airport>\S{4}):(?P<position>\S{3,4})::: 
```
# File
## Format
.ese
## Name
LPPC-Package_XXXXXXXXXXX-XXXXX-XXXX.ese
## Location
Euroscope LPPC package root folder
