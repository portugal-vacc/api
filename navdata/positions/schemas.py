from ninja import ModelSchema

from navdata.models import ControllerPosition


class ControllerPositionOut(ModelSchema):
    class Config:
        model = ControllerPosition
        model_fields = "__all__"
