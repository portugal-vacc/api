from typing import List, Dict

POSITIONS: List[Dict[str, str]] = [
    {
        'call_sign': 'LPAM_CTR',
        'name': 'Lisboa Information',
        'frequency': '123.750',
        'code': 'MI',
        'sector': '',
        'airport': 'LPAM',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPBJ_APP',
        'name': 'Beja Approach',
        'frequency': '130.100',
        'code': 'BJA',
        'sector': '',
        'airport': 'LPBJ',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPBJ_TWR',
        'name': 'Beja Tower',
        'frequency': '130.400',
        'code': 'BJT',
        'sector': '',
        'airport': 'LPBJ',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPCS_GND',
        'name': 'Cascais Ground',
        'frequency': '121.820',
        'code': 'CSG',
        'sector': '',
        'airport': 'LPCS',
        'position': 'GND',
        'rating_required': 2
    },
    {
        'call_sign': 'LPCS_TWR',
        'name': 'Cascais Tower',
        'frequency': '120.300',
        'code': 'CST',
        'sector': '',
        'airport': 'LPCS',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPFR_APP',
        'name': 'Faro Approach',
        'frequency': '119.400',
        'code': 'FRA',
        'sector': '',
        'airport': 'LPFR',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPFR_ATIS',
        'name': 'Faro ATIS',
        'frequency': '124.200',
        'code': 'FRW',
        'sector': '',
        'airport': 'LPFR',
        'position': 'ATIS',
        'rating_required': 2
    },
    {
        'call_sign': 'LPFR_GND',
        'name': 'Faro Ground',
        'frequency': '118.570',
        'code': 'FRG',
        'sector': '',
        'airport': 'LPFR',
        'position': 'GND',
        'rating_required': 2
    },
    {
        'call_sign': 'LPFR_TWR',
        'name': 'Faro Tower',
        'frequency': '120.750',
        'code': 'FRT',
        'sector': '',
        'airport': 'LPFR',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPMA_APP',
        'name': 'Madeira Approach',
        'frequency': '119.600',
        'code': 'MAA',
        'sector': '',
        'airport': 'LPMA',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPMA_ATIS',
        'name': 'Madeira ATIS',
        'frequency': '130.350',
        'code': 'MAW',
        'sector': '',
        'airport': 'LPMA',
        'position': 'ATIS',
        'rating_required': 2
    },
    {
        'call_sign': 'LPMA_S_APP',
        'name': 'Madeira Approach',
        'frequency': '120.450',
        'code': 'MAAS',
        'sector': 'S',
        'airport': 'LPMA',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPMA_TWR',
        'name': 'Madeira Tower',
        'frequency': '118.350',
        'code': 'MAT',
        'sector': '',
        'airport': 'LPMA',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPMR_APP',
        'name': 'Monte Real Approach',
        'frequency': '121.700',
        'code': 'MRA',
        'sector': '',
        'airport': 'LPMR',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPMR_TWR',
        'name': 'Monte Real Tower',
        'frequency': '122.100',
        'code': 'MRT',
        'sector': '',
        'airport': 'LPMR',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPMT_TWR',
        'name': 'Montijo Tower',
        'frequency': '134.100',
        'code': 'MTT',
        'sector': '',
        'airport': 'LPMT',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPOV_APP',
        'name': 'Ovar Approach',
        'frequency': '118.600',
        'code': 'OVA',
        'sector': '',
        'airport': 'LPOV',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPOV_TWR',
        'name': 'Ovar Tower',
        'frequency': '122.100',
        'code': 'OVT',
        'sector': '',
        'airport': 'LPOV',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPPC_CTR',
        'name': 'Lisboa Control',
        'frequency': '125.550',
        'code': 'C',
        'sector': '',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_C_CTR',
        'name': 'Lisboa Control',
        'frequency': '136.020',
        'code': 'CC',
        'sector': 'C',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_D_CTR',
        'name': 'Lisboa Control',
        'frequency': '128.900',
        'code': 'CD',
        'sector': 'D',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_E_CTR',
        'name': 'Lisboa Control',
        'frequency': '132.850',
        'code': 'CE',
        'sector': 'E',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_I_CTR',
        'name': 'Lisboa Control',
        'frequency': '132.250',
        'code': 'CI',
        'sector': 'I',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_L_CTR',
        'name': 'Lisboa Control',
        'frequency': '127.250',
        'code': 'CEU',
        'sector': 'L',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_N_CTR',
        'name': 'Lisboa Control',
        'frequency': '132.300',
        'code': 'CN',
        'sector': 'N',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_O_CTR',
        'name': 'Lisboa Control',
        'frequency': '124.350',
        'code': 'COU',
        'sector': 'O',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_S_CTR',
        'name': 'Lisboa Control',
        'frequency': '132.700',
        'code': 'CS',
        'sector': 'S',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPC_W_CTR',
        'name': 'Lisboa Control',
        'frequency': '131.320',
        'code': 'CW',
        'sector': 'W',
        'airport': 'LPPC',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPO_1_DEL',
        'name': 'Santa Maria Radio',
        'frequency': '127.900',
        'code': 'POD',
        'sector': '1',
        'airport': 'LPPO',
        'position': 'DEL',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPO_1_FSS',
        'name': 'Santa Maria Radio',
        'frequency': '124.850',
        'code': 'POR1',
        'sector': '1',
        'airport': 'LPPO',
        'position': 'FSS',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPO_2_DEL',
        'name': 'Santa Maria Radio',
        'frequency': '129.400',
        'code': 'POD2',
        'sector': '2',
        'airport': 'LPPO',
        'position': 'DEL',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPO_2_FSS',
        'name': 'Santa Maria Radio',
        'frequency': '132.000',
        'code': 'POR2',
        'sector': '2',
        'airport': 'LPPO',
        'position': 'FSS',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPO_3_FSS',
        'name': 'Santa Maria Radio',
        'frequency': '132.020',
        'code': 'POR3',
        'sector': '3',
        'airport': 'LPPO',
        'position': 'FSS',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPO_CTR',
        'name': 'Santa Maria Radar',
        'frequency': '132.150',
        'code': 'POC',
        'sector': '',
        'airport': 'LPPO',
        'position': 'CTR',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPO_FSS',
        'name': 'Santa Maria Radio',
        'frequency': '132.070',
        'code': 'POR',
        'sector': '',
        'airport': 'LPPO',
        'position': 'FSS',
        'rating_required': 5
    },
    {
        'call_sign': 'LPPR_APP',
        'name': 'Porto Approach',
        'frequency': '120.900',
        'code': 'PRA',
        'sector': '',
        'airport': 'LPPR',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPPR_ATIS',
        'name': 'Porto ATIS',
        'frequency': '124.300',
        'code': 'PRW',
        'sector': '',
        'airport': 'LPPR',
        'position': 'ATIS',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPR_DEL',
        'name': 'Porto Delivery',
        'frequency': '118.920',
        'code': 'PRD',
        'sector': '',
        'airport': 'LPPR',
        'position': 'DEL',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPR_TWR',
        'name': 'Porto Tower',
        'frequency': '118.000',
        'code': 'PRT',
        'sector': '',
        'airport': 'LPPR',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPPS_TWR',
        'name': 'Porto Santo Tower',
        'frequency': '120.050',
        'code': 'PST',
        'sector': '',
        'airport': 'LPPS',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPPT_APP',
        'name': 'Lisboa Approach',
        'frequency': '119.100',
        'code': 'PTA',
        'sector': '',
        'airport': 'LPPT',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPPT_ATIS',
        'name': 'Lisboa ATIS',
        'frequency': '124.150',
        'code': 'PTW',
        'sector': '',
        'airport': 'LPPT',
        'position': 'ATIS',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPT_DEL',
        'name': 'Lisboa Delivery',
        'frequency': '118.950',
        'code': 'PTD',
        'sector': '',
        'airport': 'LPPT',
        'position': 'DEL',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPT_F_APP',
        'name': 'Lisboa Approach',
        'frequency': '125.120',
        'code': 'PTF',
        'sector': 'F',
        'airport': 'LPPT',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPPT_GND',
        'name': 'Lisboa Ground',
        'frequency': '121.750',
        'code': 'PTG',
        'sector': '',
        'airport': 'LPPT',
        'position': 'GND',
        'rating_required': 2
    },
    {
        'call_sign': 'LPPT_TWR',
        'name': 'Lisboa Tower',
        'frequency': '118.100',
        'code': 'PTT',
        'sector': '',
        'airport': 'LPPT',
        'position': 'TWR',
        'rating_required': 3
    },
    {
        'call_sign': 'LPPT_U_APP',
        'name': 'Lisboa Control',
        'frequency': '123.970',
        'code': 'PTC',
        'sector': 'U',
        'airport': 'LPPT',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPST_APP',
        'name': 'Sintra Approach',
        'frequency': '118.600',
        'code': 'STRA',
        'sector': '',
        'airport': 'LPST',
        'position': 'APP',
        'rating_required': 4
    },
    {
        'call_sign': 'LPST_TWR',
        'name': 'Sintra Tower',
        'frequency': '119.850',
        'code': 'STT',
        'sector': '',
        'airport': 'LPST',
        'position': 'TWR',
        'rating_required': 3
    }
]

POSITIONS_DICT: Dict[str, Dict[str, str]] = {position["call_sign"]: position for position in POSITIONS}
