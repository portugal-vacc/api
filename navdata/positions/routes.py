from typing import List

from ninja import Router

from navdata.models import ControllerPosition
from navdata.positions.constants import POSITIONS_DICT
from navdata.positions.schemas import ControllerPositionOut

positions_router = Router(tags=['navdata:positions'])


@positions_router.post("/init")
def init(request):
    for position in POSITIONS_DICT.values():
        ControllerPosition.objects.update_or_create(**position)
    return {"message": "Nao Deu Merda! Yupi!"}


@positions_router.get("/", response=List[ControllerPositionOut])
def all_positions(request):
    return ControllerPosition.objects.all()
