from django.db import models

from profiles.ratings import RATINGS_CHOICES


class ControllerPosition(models.Model):
    class Meta:
        db_table = "controller_position"
        verbose_name_plural = "Controller Positions"

    call_sign = models.CharField(max_length=15, primary_key=True)
    name = models.CharField(max_length=50)
    frequency = models.CharField(max_length=7)
    code = models.CharField(max_length=4)
    sector = models.CharField(max_length=1, blank=True, null=True)
    airport = models.CharField(max_length=4)
    position = models.CharField(max_length=4)
    rating_required = models.IntegerField(choices=RATINGS_CHOICES, blank=True)

    def __str__(self):
        return f"{self.call_sign} - {self.name}"
