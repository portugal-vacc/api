from django.contrib import admin

from navdata.models import ControllerPosition


class ControllerPositionAdmin(admin.ModelAdmin):
    actions = ['required_s1', 'required_s2', 'required_s3', 'required_c1']
    list_filter = ['airport', 'position']

    @admin.action(description='Mark rating required as S1')
    def required_s1(self, request, queryset):
        queryset.update(rating_required=2)

    @admin.action(description='Mark rating required as S2')
    def required_s2(self, request, queryset):
        queryset.update(rating_required=3)

    @admin.action(description='Mark rating required as S3')
    def required_s3(self, request, queryset):
        queryset.update(rating_required=4)

    @admin.action(description='Mark rating required as C1')
    def required_c1(self, request, queryset):
        queryset.update(rating_required=5)


admin.site.register(ControllerPosition, ControllerPositionAdmin)
