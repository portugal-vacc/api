from typing import List, Dict

from ninja import Router

from meteo.ipma.services import get_metars, get_tafs

ipma_router = Router(tags=['meteo:ipma'])


@ipma_router.get("/metar", response=List[Dict[str, str]])
def metar_ipma(request, airport_icao_codes: str):
    return get_metars([airport for airport in airport_icao_codes.split(",")])


@ipma_router.get("/taf", response=List[Dict[str, str]])
def taf_ipma(request, airport_icao_codes: str):
    # TODO
    return get_tafs([airport for airport in airport_icao_codes.split(",")])


@ipma_router.get("/metar/topsky", response=str)
def metar_ipma_topsky(request, airport_icao_codes: str):
    to_return = ""
    for match in get_metars([airport for airport in airport_icao_codes.split(",")]):
        to_return += f'(prefix){match["content"]}(suffix)'
    return to_return
