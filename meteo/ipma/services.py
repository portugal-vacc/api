import re
from re import Match
from typing import List, Iterator, Dict, Literal

import httpx


def _request(airport_icao_codes: List[str], content_type: Literal["metar", "taf"]) -> str:
    params = " ".join([code.lower() for code in airport_icao_codes])
    response = httpx.post('https://brief-ng.ipma.pt/showopmetquery.php', data={"icaos": params, "type": content_type})
    response.raise_for_status()
    return response.text


def metar_parser_iterator(raw_data: str) -> Iterator[Match[str]]:
    return re.finditer(r'METAR (?P<content>(?P<airport>\S{4}).*)', raw_data)


def taf_parser_iterator(raw_data: str) -> Iterator[Match[str]]:
    # TODO
    return re.finditer(r'TAF (?P<content>(?P<airport>\S{4}) .*\n(?:.*BECMG| .*\n)*)', raw_data)


def get_metars(airport_icao_codes: List[str]) -> List[Dict[str, str]]:
    raw_data: str = _request(airport_icao_codes=airport_icao_codes, content_type="metar")
    return [metar.groupdict() for metar in metar_parser_iterator(raw_data)]


def get_tafs(airport_icao_codes: List[str]) -> List[Dict[str, str]]:
    # TODO
    raw_data: str = _request(airport_icao_codes=airport_icao_codes, content_type="taf")
    return [taf.groupdict() for taf in taf_parser_iterator(raw_data)]


if __name__ == '__main__':
    print(get_metars(["LPPT", "LPPT", "LPPR"]))
