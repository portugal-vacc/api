from ninja import Router

from meteo.ipma.router import ipma_router

meteo_router = Router(tags=['meteo'])
app_name = 'meteo'

meteo_router.add_router("/ipma/", ipma_router)
