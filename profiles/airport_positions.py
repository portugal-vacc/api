from dataclasses import dataclass
from typing import Tuple

AIRPORTS = [
    {"icao": "LPBJ", "name": "BEJA"},
    {"icao": "LPBG", "name": "BRAGANCA"},
    {"icao": "LPCS", "name": "CASCAIS"},
    {"icao": "LPCR", "name": "CORVO"},
    {"icao": "LPEV", "name": "EVORA"},
    {"icao": "LPFR", "name": "FARO"},
    {"icao": "LPFL", "name": "FLORES"},
    {"icao": "LPGR", "name": "GRACIOSA"},
    {"icao": "LPHR", "name": "HORTA"},
    {"icao": "LPLA", "name": "LAJES"},
    {"icao": "LPPT", "name": "LISBOA"},
    {"icao": "LPPC", "name": "LISBOA"},
    {"icao": "LPPO", "name": "SANTA MARIA"},
    {"icao": "LPMA", "name": "MADEIRA"},
    {"icao": "LPPI", "name": "PICO"},
    {"icao": "LPPD", "name": "PONTA DELGADA"},
    {"icao": "LPPR", "name": "PORTO"},
    {"icao": "LPPS", "name": "PORTO SANTO"},
    {"icao": "LPSJ", "name": "SAO JORGE"},
    {"icao": "LPAZ", "name": "SANTA MARIA"},
    {"icao": "LPVR", "name": "VILA REAL"}
]

STATION = [{"code": "DEL", "name": "DELIVERY"}, {"code": "GND", "name": "GROUND"}, {"code": "TWR", "name": "TOWER"},
           {"code": "APP", "name": "APPROACH"}, {"code": "CTR", "name": "CONTROL"}, {"code": "OCA", "name": "OCEANIC"}]


@dataclass
class Station:
    code: str
    name: str


@dataclass
class Airport:
    icao: str
    name: str

    def __add__(self, other: Station) -> Tuple[str, str]:
        call_sign = f"{self.icao}_{other.code}"
        name = f"{self.name} {other.name}"
        return call_sign, name


AIRPORT_POSITIONS = [Airport(**airport) + Station(**station) for airport in AIRPORTS for station in STATION]
