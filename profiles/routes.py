from ninja import Router

profiles_router = Router(tags=['profiles'])
app_name = 'profiles'


@profiles_router.get("/init")
def init(request):
    from profiles.models import AirportRating
    from profiles.airport_positions import AIRPORT_POSITIONS

    for apt_pos in AIRPORT_POSITIONS:
        AirportRating.objects.update_or_create(call_sign=apt_pos[0])
    return {"message": "Nao Deu Merda! Yupi!"}
