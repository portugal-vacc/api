from django.contrib.auth.models import User
from django.db import models

from navdata.models import ControllerPosition
from profiles.ratings import RATINGS_CHOICES, PILOT_RATINGS_CHOICES, RATINGS_DICT, PILOT_RATINGS_DICT


class VatsimProfile(models.Model):
    class Meta:
        db_table = "vatsim_profile"
        verbose_name_plural = "VATSIM Profiles"

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    rating_id: int = models.IntegerField(choices=RATINGS_CHOICES, null=False)
    pilot_rating_id: int = models.IntegerField(choices=PILOT_RATINGS_CHOICES, null=False)
    division_id: str = models.CharField(max_length=3)
    division_name: str = models.CharField(max_length=30)
    region_id: str = models.CharField(max_length=4)
    region_name: str = models.CharField(max_length=30)
    subdivision_id: str = models.CharField(max_length=3, null=True)
    subdivision_name: str = models.CharField(max_length=30, null=True)

    def __str__(self):
        return f"[{self.user.username}] {self.user.get_full_name()} - {self.rating_name} | {self.pilot_rating_name}"

    @property
    def rating_name(self) -> str:
        return RATINGS_DICT[self.rating_id].name

    @property
    def rating_description(self) -> str:
        return RATINGS_DICT[self.rating_id].description

    @property
    def pilot_rating_name(self) -> str:
        return PILOT_RATINGS_DICT[self.pilot_rating_id].name

    @property
    def pilot_rating_description(self) -> str:
        return PILOT_RATINGS_DICT[self.pilot_rating_id].description

    @property
    def is_portugal_subdivision_member(self) -> bool:
        return True if self.subdivision_id == "POR" and self.rating_id >= 2 else False


class VACCProfile(models.Model):
    class Meta:
        db_table = "vacc_profile"
        verbose_name_plural = "Portugal vACC Profiles"

    vatsim_profile = models.OneToOneField(VatsimProfile, on_delete=models.CASCADE, primary_key=True)
    controller_position_ratings = models.ManyToManyField(ControllerPosition)

    @property
    def visiting(self):
        return False if self.vatsim_profile.is_portugal_subdivision_member and self.controller_position_ratings.count() else True

    def __str__(self):
        return f"{'VISITING' if self.visiting else ''} {self.vatsim_profile.user.username}"
