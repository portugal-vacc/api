from django.contrib import admin

from profiles.models import VatsimProfile, VACCProfile


class VatsimProfileAdmin(admin.ModelAdmin):
    list_filter = ('rating_id', 'pilot_rating_id', 'division_name', 'region_name', 'subdivision_name')


admin.site.register(VatsimProfile, VatsimProfileAdmin)


class VACCProfileAdmin(admin.ModelAdmin):
    list_filter = ('controller_position_ratings',)
    filter_horizontal = ["controller_position_ratings"]


admin.site.register(VACCProfile, VACCProfileAdmin)
