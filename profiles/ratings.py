from dataclasses import dataclass

RATINGS = [
    {
        "id": -1,
        "short": "INAC",
        "long": "Inactive"
    },
    {
        "id": 0,
        "short": "SUS",
        "long": "Suspended"
    },
    {
        "id": 1,
        "short": "OBS",
        "long": "Observer"
    },
    {
        "id": 2,
        "short": "S1",
        "long": "Tower Trainee"
    },
    {
        "id": 3,
        "short": "S2",
        "long": "Tower Controller"
    },
    {
        "id": 4,
        "short": "S3",
        "long": "Senior Student"
    },
    {
        "id": 5,
        "short": "C1",
        "long": "Enroute Controller"
    },
    {
        "id": 6,
        "short": "C2",
        "long": "Controller 2 (not in use)"
    },
    {
        "id": 7,
        "short": "C3",
        "long": "Senior Controller"
    },
    {
        "id": 8,
        "short": "I1",
        "long": "Instructor"
    },
    {
        "id": 9,
        "short": "I2",
        "long": "Instructor 2 (not in use)"
    },
    {
        "id": 10,
        "short": "I3",
        "long": "Senior Instructor"
    },
    {
        "id": 11,
        "short": "SUP",
        "long": "Supervisor"
    },
    {
        "id": 12,
        "short": "ADM",
        "long": "Administrator"
    }
]

RATINGS_CHOICES = [(rating['id'], rating['short']) for rating in RATINGS]


@dataclass
class Rating:
    id: int
    short: str
    long: str

    @property
    def name(self):
        return self.short

    @property
    def description(self):
        return self.long


RATINGS_DICT = {rating["id"]: Rating(**rating) for rating in RATINGS}

PILOT_RATINGS = [
    {
        "id": 0,
        "short_name": "NEW",
        "long_name": "Basic Member"
    },
    {
        "id": 1,
        "short_name": "PPL",
        "long_name": "Private Pilot License"
    },
    {
        "id": 3,
        "short_name": "IR",
        "long_name": "Instrument Rating"
    },
    {
        "id": 7,
        "short_name": "CMEL",
        "long_name": "Commercial Multi-Engine License"
    },
    {
        "id": 15,
        "short_name": "ATPL",
        "long_name": "Airline Transport Pilot License"
    }
]

PILOT_RATINGS_CHOICES = [(rating['id'], rating['short_name']) for rating in PILOT_RATINGS]


@dataclass
class PilotRating:
    id: int
    short_name: str
    long_name: str

    @property
    def name(self):
        return self.short_name

    @property
    def description(self):
        return self.long_name


PILOT_RATINGS_DICT = {rating["id"]: PilotRating(**rating) for rating in PILOT_RATINGS}
