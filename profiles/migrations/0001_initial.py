# Generated by Django 3.2.12 on 2022-03-30 11:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('navdata', '0002_auto_20220329_0948'),
    ]

    operations = [
        migrations.CreateModel(
            name='VatsimProfile',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='auth.user')),
                ('rating_id', models.IntegerField(choices=[(-1, 'INAC'), (0, 'SUS'), (1, 'OBS'), (2, 'S1'), (3, 'S2'), (4, 'S3'), (5, 'C1'), (6, 'C2'), (7, 'C3'), (8, 'I1'), (9, 'I2'), (10, 'I3'), (11, 'SUP'), (12, 'ADM')])),
                ('pilot_rating_id', models.IntegerField(choices=[(0, 'NEW'), (1, 'PPL'), (3, 'IR'), (7, 'CMEL'), (15, 'ATPL')])),
                ('division_id', models.CharField(max_length=3)),
                ('division_name', models.CharField(max_length=30)),
                ('region_id', models.CharField(max_length=4)),
                ('region_name', models.CharField(max_length=30)),
                ('subdivision_id', models.CharField(max_length=3, null=True)),
                ('subdivision_name', models.CharField(max_length=30, null=True)),
            ],
            options={
                'verbose_name_plural': 'VATSIM Profiles',
                'db_table': 'vatsim_profile',
            },
        ),
        migrations.CreateModel(
            name='VACCProfile',
            fields=[
                ('vatsim_profile', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='profiles.vatsimprofile')),
                ('controller_position_ratings', models.ManyToManyField(to='navdata.ControllerPosition')),
            ],
            options={
                'verbose_name_plural': 'Portugal vACC Profiles',
                'db_table': 'vacc_profile',
            },
        ),
    ]
