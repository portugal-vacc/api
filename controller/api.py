from ninja import NinjaAPI

from bookings.routes import bookings_router
from controller.exceptions import ExternalServiceServerErrorResponse, ExternalServiceClientErrorResponse, \
    ExternalServiceTypeVerificationError, DualBookingError, MinimumBookingTimeError
from discord.routes import discord_router
from eaup.routes import eaup_router
from meteo.routes import meteo_router
from navdata.routes import navdata_router
from vatsim.routes import vatsim_router

my_api = NinjaAPI(title="Portugal vACC API", version="1.0.0",
                  description="Portugal vACC Application Programming Interface")

my_api.add_router("/vatsim/", vatsim_router)
my_api.add_router("/bookings/", bookings_router)
my_api.add_router("/eaup/", eaup_router)
my_api.add_router("/meteo/", meteo_router)
my_api.add_router("/navdata/", navdata_router)
my_api.add_router("/discord/", discord_router)


@my_api.exception_handler(ExternalServiceServerErrorResponse)
def external_service_unavailable(request, exc):
    return my_api.create_response(
        request,
        {"message": "External Server is having issues. Please retry later", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(ExternalServiceClientErrorResponse)
def external_data_unavailable(request, exc):
    return my_api.create_response(
        request,
        {"message": "External Server can't find the info you're looking for...", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(ExternalServiceTypeVerificationError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "External data models changed... Ah shiet, here we go again", "info": str(exc)},
        status=503,
    )


@my_api.exception_handler(DualBookingError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "Booking is conflicting with another existing booking", "info": str(exc)},
        status=403,
    )


@my_api.exception_handler(MinimumBookingTimeError)
def external_data_type_changed(request, exc):
    return my_api.create_response(
        request,
        {"message": "Booking should have minimum time 30 minutes", "info": str(exc)},
        status=403,
    )
