from __future__ import absolute_import

import os

from celery import Celery
# set the default Django settings module for the 'celery' program.
from django.core.exceptions import ValidationError

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'controller.settings')
app = Celery('api')

# Using a string here means the worker will not have to pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
from celery.signals import worker_ready


@worker_ready.connect
def at_start(sender, **k):
    from django_celery_beat.models import PeriodicTask, IntervalSchedule
    interval, created = IntervalSchedule.objects.get_or_create(every=1, period="hours")
    PeriodicTask.objects.update_or_create(name="eaup_periodic_task", task="eaup.tasks.eaup_webscrape_task",
                                          interval=interval)
    with sender.app.connection() as conn:
        sender.app.send_task('eaup.tasks.eaup_webscrape_task', connection=conn)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
