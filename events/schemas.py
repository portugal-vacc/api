from datetime import datetime
from typing import Optional, List

from pydantic import BaseModel


class EventsOrganizer(BaseModel):
    region: Optional[str]
    division: Optional[str]
    subdivision: Optional[str]
    organised_by_vatsim: bool


class EventsAirport(BaseModel):
    icao: str


class EventsRoute(BaseModel):
    departure: str
    arrival: str
    route: str


class EventSchema(BaseModel):
    id: int
    type: str
    vso_name: Optional[str]
    name: str
    link: Optional[str]
    organizers: Optional[List[EventsOrganizer]]
    airports: List[Optional[EventsAirport]]
    routes: List[Optional[EventsRoute]]
    start_time: datetime
    end_time: datetime
    short_description: str
    description: str
    banner: str

    @property
    def slug(self):
        return self.link.split("/")[-1]
