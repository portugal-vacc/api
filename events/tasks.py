from typing import List, Tuple, Any

from celery.utils.log import get_task_logger

from controller.celery import app
from events.models import Event
from events.services import portugal_events

logger = get_task_logger(__name__)


@app.task(bind=True)
def portugal_events_task(self) -> List[Tuple[str, Any]]:
    events = portugal_events()
    change_log = []
    for event in events:
        try:
            obj = Event.objects.get(id=event.id)
            obj.__dict__.update(**event.dict(exclude={"id": True}))
            obj.save()
            change_log.append(("updated", event.id))
        except Event.DoesNotExist:
            obj = Event(**event.dict())
            obj.save()
            change_log.append(("created", event.id))
    return change_log
