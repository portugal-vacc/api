# Generated by Django 3.2.12 on 2022-03-28 10:10

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('type', models.CharField(max_length=255)),
                ('vso_name', models.CharField(max_length=255, null=True)),
                ('name', models.CharField(max_length=255)),
                ('link', models.URLField(null=True)),
                ('organizers', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(), size=None)),
                ('airports', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(), size=None)),
                ('routes', django.contrib.postgres.fields.ArrayField(base_field=models.TextField(), size=None)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('short_description', models.TextField()),
                ('description', models.TextField()),
                ('banner', models.URLField()),
            ],
        ),
    ]
