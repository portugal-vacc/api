from datetime import datetime
from typing import Optional, List, Dict

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import DateTimeField, URLField, CharField, TextField, IntegerField


class Event(models.Model):
    id: int = IntegerField(primary_key=True)
    type: str = CharField(max_length=255)
    vso_name: Optional[str] = CharField(max_length=255, null=True)
    name: str = CharField(max_length=255)
    link: Optional[str] = URLField(null=True)
    organizers: Optional[List[Dict[str, str]]] = ArrayField(TextField(), default=list, null=True)
    airports: List[Optional[Dict[str, str]]] = ArrayField(TextField(), default=list, null=True)
    routes: List[Optional[Dict[str, str]]] = ArrayField(TextField(), default=list, null=True)
    start_time: datetime = DateTimeField()
    end_time: datetime = DateTimeField()
    short_description: str = TextField()
    description: str = TextField()
    banner: str = URLField()

    def __str__(self):
        return f"[{self.type}] {self.name}"
