from django.conf import settings

from controller.celery import app
from discord.guildscheduledevent.schemas import SendGuildScheduledEvent, EntityMetadata
from discord.guildscheduledevent.services import publish_guild_scheduled_event, list_scheduled_events_for_guild, \
    update_guild_scheduled_event
from events.services import portugal_events


@app.task(bind=True)
def update_or_publish_discord_guild_events(self):
    vatsim_events = portugal_events()
    discord_events = list_scheduled_events_for_guild(settings.DISCORD_MAIN_GUILD_ID)
    change_log = []
    for vatsim_event in vatsim_events:
        already_exists = [event for event in discord_events if event.slug == vatsim_event.slug]
        model = SendGuildScheduledEvent(entity_metadata=EntityMetadata(
            location=" | ".join(apt.icao for apt in vatsim_event.airports if str(apt.icao).startswith("LP"))),
            name=vatsim_event.name, scheduled_start_time=vatsim_event.start_time,
            scheduled_end_time=vatsim_event.end_time, description=vatsim_event.link, image=vatsim_event.banner)
        if already_exists:
            update_guild_scheduled_event(event_id=already_exists[0].id,
                                         guild_id=settings.DISCORD_MAIN_GUILD_ID,
                                         event=model)
            change_log.append(("Event Updated", vatsim_event.name))
        else:
            publish_guild_scheduled_event(guild_id=settings.DISCORD_MAIN_GUILD_ID,
                                          event=model)
            change_log.append(("Event Published", vatsim_event.name))
    return change_log
