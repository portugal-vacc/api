from typing import Optional, List, Any

from ninja import Schema

from discord.guild.guild import GuildSchema
from discord.guild.schemas import Channel
from discord.user.schemas import User
from discord.webhook.embed import Embed


class Webhook(Schema):
    id: int
    type: int
    guild_id: Optional[int] = None
    channel_id: Optional[int]
    user: User = None
    name: Optional[str]
    avatar: Optional[str]
    token: str = None
    application_id: Optional[int]
    source_guild: GuildSchema = None
    source_channel: Channel = None
    url: str = None


class WebhookContent(Schema):
    content: str = None
    username: str = "Portugal-vACC Bot"
    avatar_url: str = None
    tts: bool = False
    embeds: List[Embed] = None
    allowed_mentions: Any = None
    components: List[Any] = None
    attachments: List[Any] = None
