from typing import List

from pydantic import ValidationError

from controller.exceptions import ExternalServiceTypeVerificationError
from discord.api import get_api_response, post_api_response
from discord.webhook.schemas import Webhook, WebhookContent


def get_guild_webhooks(guild_id: int) -> List[Webhook]:
    data = get_api_response(endpoint=f'/guilds/{guild_id}/webhooks')
    try:
        return [Webhook(**wh) for wh in data]
    except ValidationError as e:
        raise ExternalServiceTypeVerificationError(e)


def execute_webhook(webhook_id: int, webhook_token: str, content: WebhookContent) -> bool:
    status_code = post_api_response(endpoint=f'/webhooks/{webhook_id}/{webhook_token}', content=content.json())
    return True if status_code == 204 else False
