from django.conf import settings
from ninja import Router

from discord.webhook.schemas import WebhookContent
from discord.webhook.services import execute_webhook, get_guild_webhooks

webhook_router = Router(tags=['discord:webhook'])


@webhook_router.get("/")
def webhooks(request):
    return get_guild_webhooks(guild_id=settings.DISCORD_MAIN_GUILD_ID)


@webhook_router.post("/{webhook_id}")
def execute(request, webhook_id: int, webhook_token: str, content: WebhookContent):
    return execute_webhook(webhook_id=webhook_id, webhook_token=webhook_token, content=content)
