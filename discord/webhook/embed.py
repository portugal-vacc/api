from datetime import datetime
from typing import List

from ninja import Schema


class Footer(Schema):
    text: str
    icon_url: str = None
    proxy_icon_url: str = None


class Image(Schema):
    url: str
    proxy_url: str = None
    height: int = None
    width: int = None


class Thumbnail(Schema):
    url: str
    proxy_url: str = None
    height: int = None
    width: int = None


class Video(Schema):
    url: str = None
    proxy_url: str = None
    height: int = None
    width: int = None


class Provider(Schema):
    name: str = None
    url: str = None


class Author(Schema):
    name: str
    url: str = None
    icon_url: str = None
    proxy_icon_url: str = None


class EmbedField(Schema):
    name: str
    value: str
    inline: bool = False


class Embed(Schema):
    title: str = None
    type: str = "rich"
    description: str = None
    url: str = None
    timestamp: datetime = None
    color: int = None
    footer: Footer = None
    image: Image = None
    thumbnail: Thumbnail = None
    video: Video = None
    provider: Provider = None
    author: Author = None
    fields: List[EmbedField] = None
