from datetime import datetime
from typing import Optional

from ninja import Schema

from discord.guild.schemas import GuildMember


class VoiceState(Schema):
    """https://discord.com/developers/docs/resources/voice#voice-state-object"""
    guild_id: int = None
    channel_id: Optional[int]
    user_id: int
    member: GuildMember = None
    session_id: str
    deaf: bool
    mute: bool
    self_deaf: bool
    self_mute: bool
    self_stream: bool = None
    self_video: bool
    suppress: bool
    request_to_speak_timestamp: Optional[datetime]
