from typing import Optional

from ninja import Schema


class RoleTags(Schema):
    """https://discord.com/developers/docs/topics/permissions#role-object-role-tags-structure"""
    bot_id: int = None
    integration_id: int = None
    premium_subscriber: Optional[bool] = None


class Role(Schema):
    """https://discord.com/developers/docs/topics/permissions#role-object"""
    id: int
    name: str
    color: int
    hoist: bool
    icon: Optional[str] = None
    unicode_emoji: Optional[str] = None
    position: int
    permissions: str
    managed: bool
    mentionable: bool
    tags: RoleTags = None
