import httpx
from django.conf import settings

from controller.exceptions import ExternalServiceClientErrorResponse, ExternalServiceServerErrorResponse


def get_api_response(endpoint: str) -> dict:
    response = httpx.get(f"https://discord.com/api/v9{endpoint}",
                         headers={"Authorization": f"Bot {settings.DISCORD_BOT_TOKEN}"})
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse(response.text)
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse(response.text)
    return response.json()


def post_api_response(endpoint: str, content: str = "") -> int:
    response = httpx.post(f"https://discord.com/api/v9{endpoint}",
                          headers={"Authorization": f"Bot {settings.DISCORD_BOT_TOKEN}",
                                   "Content-Type": "application/json"},
                          content=content)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse(response.text)
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse(response.text)
    return response.status_code


def put_api_response(endpoint: str, content: str = "") -> int:
    response = httpx.put(f"https://discord.com/api/v9{endpoint}",
                         headers={"Authorization": f"Bot {settings.DISCORD_BOT_TOKEN}",
                                  "Content-Type": "application/json"},
                         content=content)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse(response.text)
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse(response.text)
    return response.status_code


def delete_api_response(endpoint: str) -> int:
    response = httpx.delete(f"https://discord.com/api/v9{endpoint}",
                            headers={"Authorization": f"Bot {settings.DISCORD_BOT_TOKEN}"})
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse(response.text)
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse(response.text)
    return response.status_code


def patch_api_response(endpoint: str, content: str = "") -> int:
    response = httpx.patch(f"https://discord.com/api/v9{endpoint}",
                           headers={"Authorization": f"Bot {settings.DISCORD_BOT_TOKEN}",
                                    "Content-Type": "application/json"},
                           content=content)
    if str(response.status_code).startswith("4"):
        raise ExternalServiceClientErrorResponse(response.text)
    if str(response.status_code).startswith("5"):
        raise ExternalServiceServerErrorResponse(response.text)
    return response.status_code
