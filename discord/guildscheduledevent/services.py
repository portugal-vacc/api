from typing import List

from cachetools import cached, TTLCache
from pydantic import ValidationError

from controller.exceptions import ExternalServiceTypeVerificationError
from discord.api import get_api_response, post_api_response, patch_api_response
from discord.guildscheduledevent.schemas import GuildScheduledEvent, SendGuildScheduledEvent


@cached(cache=TTLCache(maxsize=16, ttl=600))
def list_scheduled_events_for_guild(guild_id: int) -> List[GuildScheduledEvent]:
    data = get_api_response(endpoint=f'/guilds/{guild_id}/scheduled-events?with_user_count=True')
    try:
        return [GuildScheduledEvent(**e) for e in data]
    except ValidationError as e:
        raise ExternalServiceTypeVerificationError(e)


def publish_guild_scheduled_event(guild_id: int, event: SendGuildScheduledEvent) -> bool:
    status_code = post_api_response(endpoint=f'/guilds/{guild_id}/scheduled-events', content=event.json())
    return True if status_code == 200 else False


def update_guild_scheduled_event(event_id: int, guild_id: int, event: SendGuildScheduledEvent) -> bool:
    status_code = patch_api_response(endpoint=f'/guilds/{guild_id}/scheduled-events/{event_id}',
                                     content=event.json())
    return True if status_code == 200 else False
