import base64
from datetime import datetime
from enum import Enum
from typing import Optional

import httpx
from ninja import Schema
from pydantic import validator

from discord.user.schemas import User


class PrivacyLevel(Enum):
    """https://discord.com/developers/docs/resources/guild-scheduled-event#guild-scheduled-event-object-guild-scheduled-event-privacy-level"""
    GUILD_ONLY = 2


class EventStatus(Enum):
    """https://discord.com/developers/docs/resources/guild-scheduled-event#guild-scheduled-event-object-guild-scheduled-event-status"""
    SCHEDULED = 1
    ACTIVE = 2
    COMPLETED = 3
    CANCELED = 4


class ScheduledEntityType(Enum):
    """https://discord.com/developers/docs/resources/guild-scheduled-event#guild-scheduled-event-object-guild-scheduled-event-entity-types"""
    STAGE_INSTANCE = 1
    VOICE = 2
    EXTERNAL = 3


class EntityMetadata(Schema):
    """https://discord.com/developers/docs/resources/guild-scheduled-event#guild-scheduled-event-object-guild-scheduled-event-entity-metadata"""
    location: str = None


class GuildScheduledEvent(Schema):
    """https://discord.com/developers/docs/resources/guild-scheduled-event#guild-scheduled-event-object"""
    id: int
    guild_id: int
    channel_id: Optional[int]
    creator_id: Optional[int] = None
    name: str
    description: Optional[str] = None
    scheduled_start_time: datetime
    scheduled_end_time: datetime
    privacy_level: PrivacyLevel
    status: EventStatus
    entity_type: ScheduledEntityType
    entity_id: Optional[int]
    entity_metadata: Optional[EntityMetadata]
    creator: User = None
    user_count: int = None
    image: Optional[str] = None

    @property
    def slug(self):
        return self.description.split("/")[-1]


class SendGuildScheduledEvent(Schema):
    entity_metadata: EntityMetadata
    name: str
    privacy_level: PrivacyLevel = PrivacyLevel.GUILD_ONLY
    scheduled_start_time: datetime
    scheduled_end_time: datetime
    description: Optional[str]
    entity_type: ScheduledEntityType = ScheduledEntityType.EXTERNAL
    image: Optional[str]  # pass url of banner here

    @validator('name')
    def name_must_be_in_title_form(cls, v):
        return v.title()

    @validator('image')
    def transform_image_url_into_base64(cls, v):
        return f"data:image/png;base64,{base64.b64encode(httpx.get(v).content).decode('utf-8')}"

    class Config:
        json_encoders = {
            datetime: lambda v: v.isoformat()
        }

