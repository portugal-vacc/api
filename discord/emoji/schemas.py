from typing import Optional, List

from ninja import Schema

from discord.permissions.schemas import Role
from discord.user.schemas import User


class Emoji(Schema):
    """https://discord.com/developers/docs/resources/emoji#emoji-object"""
    id: Optional[int]
    name: Optional[str]
    roles: List[Optional[Role]] = None
    user: User = None
    require_colons: bool = None
    managed: bool = None
    animated: bool = None
    available: bool = None
