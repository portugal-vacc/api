from ninja import Router

from discord.guild.routes import guild_router
from discord.webhook.routes import webhook_router

discord_router = Router(tags=['discord'])
app_name = 'discord'

discord_router.add_router("/guild/", guild_router)
discord_router.add_router("/webhook/", webhook_router)
