from typing import List

from django.conf import settings
from ninja import Router

from discord.guild.guild import GuildSchema
from discord.guild.schemas import Channel, Ban
from discord.guild.services import get_guild, modify_guild_member, get_guild_channels, get_guild_bans

guild_router = Router(tags=['discord:guild'])


@guild_router.get("/", response=GuildSchema)
def guild(request):
    return get_guild(guild_id=settings.DISCORD_MAIN_GUILD_ID)


@guild_router.get("/channels", response=List[Channel])
def channels(request):
    return get_guild_channels(guild_id=settings.DISCORD_MAIN_GUILD_ID)


@guild_router.patch("/members/{user_id}")
def guild_member(request, user_id: int, nickname: str = "", roles: List[int] = []):
    return {"message": "success"} if modify_guild_member(guild_id=settings.DISCORD_MAIN_GUILD_ID, user_id=user_id,
                                                         nickname=nickname, roles=roles) else {
        "message": "nope, something broken"}


@guild_router.get("/bans", response=List[Ban])
def guild_bans(request):
    return get_guild_bans(guild_id=settings.DISCORD_MAIN_GUILD_ID)
