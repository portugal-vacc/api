from datetime import datetime
from typing import Optional, List, Literal

from ninja import Schema

from discord.user.schemas import User

"""https://discord.com/developers/docs/resources/guild#guild-object-guild-features"""
guild_feature_type = Literal[
    "ANIMATED_ICON", "BANNER", "COMMERCE", "COMMUNITY", "DISCOVERABLE", "FEATURABLE", "INVITE_SPLASH",
    "MEMBER_VERIFICATION_GATE_ENABLED", "MONETIZATION_ENABLED", "MORE_STICKERS", "NEWS", "PARTNERED", "PREVIEW_ENABLED",
    "PRIVATE_THREADS", "ROLE_ICONS", "SEVEN_DAY_THREAD_ARCHIVE", "THREE_DAY_THREAD_ARCHIVE", "TICKETED_EVENTS_ENABLED",
    "VANITY_URL", "VERIFIED", "VIP_REGIONS", "WELCOME_SCREEN_ENABLED", "THREADS_ENABLED", "THREE_DAY_THREAD_ARCHIVE",
    "NEW_THREAD_PERMISSIONS"]


class GuildMember(Schema):
    """https://discord.com/developers/docs/resources/guild#guild-member-object"""
    user: User = None
    nick: Optional[str] = None
    avatar: Optional[str] = None
    roles: List[int]
    joined_at: datetime
    premium_since: Optional[datetime] = None
    deaf: bool
    mute: bool
    pending: bool = None
    permissions: str = None
    communication_disabled_until: Optional[datetime] = None


class WelcomeScreenChannel(Schema):
    """https://discord.com/developers/docs/resources/guild#welcome-screen-object-welcome-screen-channel-structure"""
    channel_id: int
    description: str
    emoji_id: Optional[int]
    emoji_name: Optional[str]


class WelcomeScreen(Schema):
    """https://discord.com/developers/docs/resources/guild#welcome-screen-object"""
    description: Optional[str]
    welcome_channels: List[WelcomeScreenChannel]


class OverWrite(Schema):
    """https://discord.com/developers/docs/resources/channel#overwrite-object"""
    id: int
    type: int
    allow: str
    deny: str


class ThreadMetadata(Schema):
    """https://discord.com/developers/docs/resources/channel#thread-metadata-object"""
    archived: bool
    auto_archive_duration: int
    archive_timestamp: datetime
    locked: bool
    invitable: bool = None
    create_timestamp: Optional[datetime] = None


class ThreadMember(Schema):
    """https://discord.com/developers/docs/resources/channel#thread-member-object"""
    id: int = None
    user_id: int = None
    join_timestamp: datetime
    flags: int


class Channel(Schema):
    """https://discord.com/developers/docs/resources/channel#channel-object"""
    id: int
    type: int
    guild_id: int = None
    position: int = None
    permission_overwrites: List[Optional[OverWrite]] = None
    name: str = None
    topic: Optional[str] = None
    nsfw: bool = None
    last_message_id: Optional[int] = None
    bitrate: int = None
    user_limit: int = None
    rate_limit_per_user: int = None
    recipients: List[Optional[User]] = None
    icon: Optional[str] = None
    owner_id: int = None
    application_id: int = None
    parent_id: Optional[int] = None
    last_pin_timestamp: Optional[datetime] = None
    rtc_region: Optional[str] = None
    video_quality_mode: int = None
    message_count: int = None
    member_count: int = None
    thread_metadata: ThreadMetadata = None
    member: ThreadMember = None
    default_auto_archive_duration: int = None
    permissions: str = None


class Ban(Schema):
    reason: Optional[str]
    user: User
