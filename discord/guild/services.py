import json
from typing import List

from pydantic import ValidationError

from controller.exceptions import ExternalServiceTypeVerificationError
from discord.api import get_api_response, patch_api_response, put_api_response, delete_api_response
from discord.guild.guild import GuildSchema
from discord.guild.schemas import Channel, Ban


def get_guild(guild_id: int) -> GuildSchema:
    data = get_api_response(endpoint=f'/guilds/{guild_id}?with_counts=True')
    try:
        return GuildSchema(**data)
    except ValidationError as e:
        raise ExternalServiceTypeVerificationError(e)


def get_guild_channels(guild_id: int) -> List[Channel]:
    data = get_api_response(endpoint=f'/guilds/{guild_id}/channels')
    try:
        return [Channel(**c) for c in data]
    except ValidationError as e:
        raise ExternalServiceTypeVerificationError(e)


def modify_guild_member(guild_id: int, user_id: int, nickname: str = "", roles: List[int] = []) -> bool:
    status_code = patch_api_response(endpoint=f'/guilds/{guild_id}/members/{user_id}',
                                     content=json.dumps({"nick": nickname, "roles": roles}))
    return True if status_code == 200 else False


def get_guild_bans(guild_id: int) -> List[Ban]:
    data = get_api_response(endpoint=f'/guilds/{guild_id}/bans')
    try:
        return [Ban(**ban) for ban in data]
    except ValidationError as e:
        raise ExternalServiceTypeVerificationError(e)


def create_guild_ban(guild_id: int, user_id: int) -> bool:
    status_code = put_api_response(endpoint=f'/guilds/{guild_id}/bans/{user_id}')
    return True if status_code == 204 else False


def remove_guild_ban(guild_id: int, user_id: int) -> bool:
    status_code = delete_api_response(endpoint=f'/guilds/{guild_id}/bans/{user_id}')
    return True if status_code == 204 else False
