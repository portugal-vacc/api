from datetime import datetime
from typing import Optional, List

from ninja import Schema

from discord.emoji.schemas import Emoji
from discord.gateway.schemas import PresenceUpdate
from discord.guild.schemas import guild_feature_type, GuildMember, WelcomeScreen, Channel
from discord.guildscheduledevent.schemas import GuildScheduledEvent
from discord.permissions.schemas import Role
from discord.stage.schemas import StageInstance
from discord.sticker.schemas import Sticker
from discord.voice.schemas import VoiceState


class GuildSchema(Schema):
    """https://discord.com/developers/docs/resources/guild#guild-object"""
    id: int
    name: str
    icon: Optional[str]
    icon_hash: Optional[str] = None
    splash: Optional[str]
    discovery_splash: Optional[str]
    owner: Optional[bool] = None
    owner_id: int
    permissions: Optional[str] = None
    afk_channel_id: Optional[str]
    afk_timeout: int
    widget_enabled: Optional[bool] = None
    widget_channel_id: Optional[int] = None
    verification_level: int
    default_message_notifications: int
    explicit_content_filter: int
    roles: List[Optional[Role]]
    emojis: List[Optional[Emoji]]
    features: List[Optional[guild_feature_type]]
    mfa_level: int
    application_id: Optional[int]
    system_channel_id: Optional[int]
    system_channel_flags: int
    rules_channel_id: Optional[int]
    joined_at: Optional[datetime] = None
    large: Optional[bool] = None
    unavailable: Optional[bool] = None
    member_count: Optional[int] = None
    voice_states: List[Optional[VoiceState]] = None
    members: List[Optional[GuildMember]] = None
    channels: List[Optional[Channel]] = None
    threads: List[Optional[Channel]] = None
    presences: List[Optional[PresenceUpdate]] = None
    max_presences: Optional[int] = None
    max_members: int = None
    vanity_url_code: Optional[str]
    description: Optional[str]
    banner: Optional[str]
    premium_tier: int
    premium_subscription_count: int = None
    preferred_locale: str
    public_updates_channel_id: Optional[int]
    max_video_channel_users: int = None
    approximate_member_count: int = None
    approximate_presence_count: int = None
    welcome_screen: WelcomeScreen = None
    nsfw_level: int
    stage_instances: List[Optional[StageInstance]] = None
    stickers: List[Optional[Sticker]] = None
    guild_scheduled_events: List[Optional[GuildScheduledEvent]] = None
    premium_progress_bar_enabled: bool
