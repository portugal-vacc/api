from typing import Optional

from ninja import Schema


class User(Schema):
    """https://discord.com/developers/docs/resources/user#user-object"""
    id: int
    username: str
    discriminator: str
    avatar: Optional[str]
    bot: bool = None
    system: bool = None
    mfa_enabled: bool = None
    banner: Optional[str] = None
    accent_color: Optional[int] = None
    locale: str = None
    verified: bool = None
    email: Optional[str] = None
    flags: int = None
    premium_type: int = None
    public_flags: int = None
