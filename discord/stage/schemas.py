from enum import Enum
from typing import Optional

from ninja import Schema


class StagePrivacyLevel(Enum):
    PUBLIC = 1
    GUILD_ONLY = 2


class StageInstance(Schema):
    """https://discord.com/developers/docs/resources/stage-instance#stage-instance-object"""
    id: int
    guild_id: int
    channel_id: int
    topic: str
    privacy_level: StagePrivacyLevel
    discoverable_disabled: bool
    guild_scheduled_event_id: Optional[int]
