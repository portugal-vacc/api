from typing import List, Optional

from ninja import Schema

from discord.emoji.schemas import Emoji
from discord.user.schemas import User


class TimeStamps(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object-activity-timestamps"""
    start: int = None
    end: int = None


class ActivityParty(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object-activity-party"""
    id: str = None
    size: List[Optional[int]] = None


class ActivityAssets(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object-activity-assets"""
    large_image: str = None
    large_text: str = None
    small_image: str = None
    small_text: str = None


class ActivitySecrets(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object-activity-secrets"""
    join: str = None
    spectate: str = None
    match: str = None


class ActivityButtons(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object-activity-buttons"""
    label: str
    url: str


class Activity(Schema):
    """https://discord.com/developers/docs/topics/gateway#activity-object"""
    name: str
    type: int
    url: Optional[str] = None
    created_at: int
    timestamps: TimeStamps
    application_id: int = None
    details: Optional[str] = None
    state: Optional[str] = None
    emoji: Optional[Emoji] = None
    party: ActivityParty = None
    assets: ActivityAssets = None
    secrets: ActivitySecrets = None
    instance: bool = None
    flags: int = None
    buttons: List[Optional[ActivityButtons]] = None


class ClientStatus(Schema):
    """https://discord.com/developers/docs/topics/gateway#client-status-object"""
    desktop: str = None
    mobile: str = None
    web: str = None


class PresenceUpdate(Schema):
    """https://discord.com/developers/docs/topics/gateway#presence-update"""
    user: User
    guild_id: int
    status: str
    activities: List[Optional[Activity]]
    client_status: ClientStatus
